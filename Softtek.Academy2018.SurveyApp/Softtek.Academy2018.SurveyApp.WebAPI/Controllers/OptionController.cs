﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Business.Implementations;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api")]
    public class OptionController : ApiController
    {
        private readonly IOptionsService _optionService;
        public OptionController(OptionService userService)
        {
            _optionService = userService;
        }
        [Route("Option")]
        [HttpPost]
        public IHttpActionResult CreateOption([FromBody] OptionDTO optionDTO)
        {
            Option option = new Option
            {
                Text = optionDTO.Text,
                CreatedDate = optionDTO.CreatedDate,
                ModifiedDate = optionDTO.ModifiedDate
            };
            int id = _optionService.Add(option);
            if (id <= 0) return BadRequest("Unable to create user");
            var payload = new { userid = id };
            return Ok(payload);
        }
    }

    public class OptionDTO
    {
        public string Text { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}
