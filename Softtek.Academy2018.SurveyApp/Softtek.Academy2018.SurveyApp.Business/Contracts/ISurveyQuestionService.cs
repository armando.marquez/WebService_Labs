﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    public interface ISurveyQuestionService
    {
        bool AddQuestion(int SurveyId,int QuestionId);
        bool RemoveQuestion(int QuestionId);
        ICollection<Question> GetQuestions(int SurveyId);
        ICollection<Option> GetOptionByQuestionOnSurvey(int SruveyId, int QuestionId);
    }
}
