﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Contracts
{
    public interface IQuestionService
    {
        int Add(Question survey);
        Question Get(int id);
        ICollection<Question> GetAll();
        bool RemoveOption();
        bool Update(Question survey);
    }
}
