﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Business.Implementations
{
    public class SurveyQuestionService : ISurveyQuestionService
    {
        public bool AddQuestion(int SurveyId, int QuestionId)
        {
            throw new NotImplementedException();
        }

        public ICollection<Option> GetOptionByQuestionOnSurvey(int SruveyId, int QuestionId)
        {
            throw new NotImplementedException();
        }

        public ICollection<Question> GetQuestions(int SurveyId)
        {
            throw new NotImplementedException();
        }

        public bool RemoveQuestion(int QuestionId)
        {
            throw new NotImplementedException();
        }
    }
}
