﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Business.Implementations
{
    public class SurveyInterface : ISurveyInterface
    {
        public int Add(Survey survey)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Survey survey)
        {
            throw new NotImplementedException();
        }

        public Survey Get(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<Survey> LastTen()
        {
            throw new NotImplementedException();
        }

        public bool Update(Survey survey)
        {
            throw new NotImplementedException();
        }
    }
}
