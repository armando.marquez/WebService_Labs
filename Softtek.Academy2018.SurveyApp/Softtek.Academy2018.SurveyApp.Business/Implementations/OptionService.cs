﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Implementations;

namespace Softtek.Academy2018.SurveyApp.Business.Implementations
{
    public class OptionService : IOptionsService
    {
        private readonly IOptionRepository _optionRepository;
        public OptionService(IOptionRepository optionRepository)
        {
            _optionRepository = optionRepository;
        }

        public int Add(Option option)
        {
            if (_optionRepository.Exist(option.Text))
                return 0;
            return _optionRepository.Add(option);
        }

        public Option Get(int id)
        {
            return _optionRepository.Get(id);
        }        

        public bool Update(int id)
        {
            throw new NotImplementedException();
        }
    }
}
