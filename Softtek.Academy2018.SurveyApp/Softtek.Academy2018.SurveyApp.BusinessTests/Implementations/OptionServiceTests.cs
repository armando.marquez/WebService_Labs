﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Business.Implementations;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Business.Implementations.Tests
{
    [TestClass()]
    public class OptionServiceTests
    {
        [TestMethod()]
        public void AddTest()
        {
            int expected = 1;

            Mock<IOptionRepository> respository = new Mock<IOptionRepository>();

            respository.Setup(x => x.Exist(It.IsAny<string>())).Returns(false);

            IOptionsService service = new OptionService(respository.Object);

            Option  option = new Option 
            {
                 Text="cosa"                 
            };

            int result = service.Add(option);

            Assert.AreEqual(expected, result);
        }
    }
}