﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionTypeRepository : IQuestionTypeRepository
    {
        public QuestionType Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.QuetionTypes.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public ICollection<QuestionType> GetAll()
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.QuetionTypes.ToList();
            }
        }
    }
}
