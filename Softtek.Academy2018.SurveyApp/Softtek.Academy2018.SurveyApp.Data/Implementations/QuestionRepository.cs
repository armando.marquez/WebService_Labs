﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionRepository : IQuestionRepository
    {
        public int Add(Question question)
        {
            using (var context = new SurveyDbContext())
            {
                question.CreatedDate = DateTime.Now;
                question.ModifiedDate = null;
                question.IsActive = true;
                context.Questions.Add(question);
                context.SaveChanges();
                return question.Id;
            }
        }

        public bool AddOption(int QuestionId, int OptionId)
        {
            using (var context = new SurveyDbContext())
            {
                Option currentOption = context.Options.SingleOrDefault(x => x.Id == OptionId);

                if (currentOption == null) return false;

                Question currentQuestion = context.Questions.SingleOrDefault(x => x.Id == QuestionId);

                if (currentQuestion == null) return false;

                currentQuestion.Options.Add(currentOption);

                context.SaveChanges();

                return true;
            }
        }

        //Delete no se implementa
        public bool Delete(Question question)
        {
            throw new NotImplementedException();
        }

        public Question Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Questions.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public ICollection<Question> GetAll()
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Questions.ToList();
            }
        }

        public ICollection<Option> GetOptionByQuestion(int QuestionId)
        {
            using (var context = new SurveyDbContext())
            {
                Question currentQuestion = context.Questions.SingleOrDefault(x => x.Id == QuestionId);
                if (currentQuestion == null) return null;
                return currentQuestion.Options;
            }
        }

        public bool RemoveOption(int QuestionId, int OptionId)
        {
            using (var context = new SurveyDbContext())
            {
                Option currentOption = context.Options.SingleOrDefault(x => x.Id == OptionId);

                if (currentOption == null) return false;

                Question currentQuestion = context.Questions.SingleOrDefault(x => x.Id == QuestionId);

                if (currentQuestion == null) return false;

                currentQuestion.Options.Remove(currentOption);

                context.SaveChanges();

                return true;
            }
        }

        public bool Update(Question question)
        {
            using (var ctx = new SurveyDbContext())
            {
                Question currentQuestion = ctx.Questions.SingleOrDefault(x => x.Id == question.Id);

                if (currentQuestion == null) return false;

                currentQuestion.Text = question.Text;
                currentQuestion.QuestionTypeId = question.QuestionTypeId;                
                currentQuestion.ModifiedDate = DateTime.Now;

                ctx.Entry(currentQuestion).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }

        public bool hasOption(int opt)
        {
           
            return false;
        }
            
    }
}
