﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class SurveyRepository : ISurveyRepository  
    {
        public int Add(Survey survey)
        {
            using (var ctx = new SurveyDbContext())
            {
                survey.CreatedDate = DateTime.Now;
                survey.ModifiedDate = null;
                survey.IsArchived = false;
                ctx.Surveys.Add(survey);
                ctx.SaveChanges();
                return survey.Id;
            }
        }

        public bool AddQuestion(int SurveyId,int QuestionId)
        {
            using (var context = new SurveyDbContext())
            {
                Survey currentSurvey = context.Surveys.SingleOrDefault(x => x.Id == SurveyId);

                if (currentSurvey == null) return false;

                Question currentUser = context.Questions.SingleOrDefault(x => x.Id == QuestionId);

                if (currentUser == null) return false;

                currentSurvey.Questions.Add(currentUser);

                context.SaveChanges();

                return true;
            }
        }

        // este no se va a implementar pero que horror quitarlo
        public bool Delete(Survey survey)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            using (var context = new SurveyDbContext())
            {
                Survey currentSurvey = context.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == id);

                if (currentSurvey == null) return false;

                currentSurvey.IsArchived = true;

                context.Entry(currentSurvey).State = EntityState.Modified;

                context.SaveChanges();

                return true;
            }
        }

        public Survey Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public ICollection<Option> GetOptionByQuestionOnSurvey(int SurveyId, int QuestionId)
        {
            using (var context = new SurveyDbContext())
            {
                Survey currentSurvey = context.Surveys.SingleOrDefault(x => x.Id == SurveyId);

                if (currentSurvey == null) return null;

                Question currentUser = context.Questions.SingleOrDefault(x => x.Id == QuestionId);

                if (currentUser == null) return null;

                return currentUser.Options;
            }
        }

        public ICollection<Question> GetQuestions(int SurveyId)
        {
            using (var context = new SurveyDbContext())
            {               
                Survey currentSurvey = context.Surveys.SingleOrDefault(x => x.Id == SurveyId);
                if (currentSurvey == null) return null;
                return currentSurvey.Questions;
            }
        }

        public ICollection<Survey> LastTen()
        {
            throw new NotImplementedException();
        }

        public bool RemoveQuestion(int SurveyId,int QuestionId)
        {
            using (var context = new SurveyDbContext())
            {
                Survey currentSurvey = context.Surveys.SingleOrDefault(x => x.Id == SurveyId);

                if (currentSurvey == null) return false;

                Question currentUser = context.Questions.SingleOrDefault(x => x.Id == QuestionId);

                if (currentUser == null) return false;

                currentSurvey.Questions.Remove(currentUser);

                context.SaveChanges();

                return true;
            }
        }

        public bool Update(Survey survey)
        {
            using (var ctx = new SurveyDbContext())
            {
                Survey currentSurvey = ctx.Surveys.SingleOrDefault(x => x.Id == survey.Id);

                if (currentSurvey == null) return false;

                currentSurvey.Title = survey.Title;
                currentSurvey.Description = survey.Description;
                currentSurvey.ModifiedDate = DateTime.Now;

                ctx.Entry(currentSurvey).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }
       
    }
}
