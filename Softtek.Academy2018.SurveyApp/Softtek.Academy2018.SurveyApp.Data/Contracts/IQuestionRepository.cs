﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Collections.Generic;

namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface IQuestionRepository : IGenericRepository<Question>
    {
        //No delete
        ICollection<Question> GetAll();
        bool AddOption(int QuestionId,int OptionId);
        bool RemoveOption(int QuestionId, int OptionId);
        ICollection<Option> GetOptionByQuestion(int QuestionId);
        
    }
}
