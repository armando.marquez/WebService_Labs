﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface ISurveyRepository : IGenericRepository<Survey>
    {
        bool Delete(int id); 
        bool AddQuestion(int SurveyId,int QuestionId);
        bool RemoveQuestion(int SurveyId,int QuestionId);
        ICollection<Survey> LastTen();
        ICollection<Question> GetQuestions(int SurveyId);
        ICollection<Option> GetOptionByQuestionOnSurvey(int SruveyId, int QuestionId);
    }
}
